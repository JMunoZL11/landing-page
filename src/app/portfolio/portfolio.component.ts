import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  // public isActive: boolean = false;

  public projects: Project[] = [
    new Project(
      "Project One",
      "Adipisci quas repellat sed. Quasi quaerat aut nam possimus vitae dignissimos, sapiente est atque tenetur.",
      "https://material.angular.io/assets/img/examples/shiba2.jpg"
    ),
    new Project(
      "Project Two",
      "Adipisci quas repellat sed. Quasi quaerat aut nam possimus vitae dignissimos, sapiente est atque tenetur.",
      "https://material.angular.io/assets/img/examples/shiba2.jpg"
    ),
    new Project(
      "Project Three",
      "Adipisci quas repellat sed. Quasi quaerat aut nam possimus vitae dignissimos, sapiente est atque tenetur.",
      "https://material.angular.io/assets/img/examples/shiba2.jpg"
    ),
  ];

  constructor() { }

  ngOnInit() {
  }
}

class Project {
  constructor(
    public title: string,
    public description: string,
    public image: string,
    public hover: boolean = false,
  ) { }
}
