import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  services: Service[] = [
    new Service(
      "Business Application",
      "Lorem ipsum dolor sit amet conse ctetur adipi sicing elit. Doloribus numquam quis.",
      "dashboard"
    ),
    new Service(
      "Custom System Integration",
      "Lorem ipsum dolor sit amet conse ctetur adipi sicing elit. Doloribus numquam quis.",
      "perm_data_setting"
    ),
    new Service(
      "Database Administration",
      "Lorem ipsum dolor sit amet conse ctetur adipi sicing elit. Doloribus numquam quis.",
      "storage"
    ),
    new Service(
      "Custom Mobile Application",
      "Lorem ipsum dolor sit amet conse ctetur adipi sicing elit. Doloribus numquam quis.",
      "stay_primary_portrait"
    ),
  ];

  constructor() { }

  ngOnInit() {
  }

}

class Service {
  constructor(
    public title: string,
    public description: string,
    public icon: string,
    public hover: boolean = false,
  ) {}
}
