import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  plans: Plan[] = [
    new Plan(
      "Developer",
      "For New Developers",
      "FREE",
      [
        "10GB of Bandwidth",
        "Max 50 connection",
        "512MB RAM",
        "Unlimited access",
        "Unlimited User",
        "Data analytics",
      ]
    ),
    new Plan(
      "Starter",
      "For Professional Developers",
      "$ 30 /Mo",
      [
        "100GB of Bandwidth",
        "Max 500 connection",
        "1GB RAM",
        "Unlimited access",
        "Unlimited User",
        "Data analytics",
      ]
    ),
    new Plan(
      "Business",
      "For Small Businesses",
      "$ 60 /Mo",
      [
        "100GB of Bandwidth",
        "Max 1500 connection",
        "2GB RAM",
        "Unlimited access",
        "Unlimited User",
        "Data analytics",
      ]
    ),
    new Plan(
      "Enterprise",
      "For Large companies",
      "$ 160 /Mo",
      [
        "1000GB of Bandwidth",
        "Max 5000 connection",
        "8GB RAM",
        "Unlimited access",
        "Unlimited User",
        "Data analytics",
      ]
    ),
  ];

  constructor() { }

  ngOnInit() {
  }

}

class Plan {
  constructor(
    public title: string,
    public subtitle: string,
    public price: string,
    public features: string[],
    public hover: boolean = false,
  ) {}
}
