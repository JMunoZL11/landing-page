import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatCardModule, MatDividerModule, MatSlideToggleModule, MatListModule, MatInputModule, MatSidenavModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatListModule,
    MatInputModule,
    MatSidenavModule,
    LayoutModule,
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatListModule,
    MatInputModule,
    MatSidenavModule,
    LayoutModule,
  ]
})
export class MaterialModule { }