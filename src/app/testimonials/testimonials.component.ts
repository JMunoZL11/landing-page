import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { NguCarouselConfig, NguCarousel } from '@ngu/carousel';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss'],
})
export class TestimonialsComponent implements AfterViewInit {

  carouselItems = [
    new TestimonialCard(
      "../../assets/images/arion-stand-alone-logo.png",
      '"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius, atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."',
      "/../../assets/images/user1.png",
      "Jane Doe",
      "CEO"
    ),
    new TestimonialCard(
      "../../assets/images/overleaf_logo.png",
      '"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius, atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."',
      "/../../assets/images/user2.png",
      "John Doe",
      "Product Manager"
    ),
    new TestimonialCard(
      "../../assets/images/arion-stand-alone-logo.png",
      '"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius, atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."',
      "/../../assets/images/user1.png",
      "Jane Doe",
      "CEO"
    ),
    new TestimonialCard(
      "../../assets/images/overleaf_logo.png",
      '"Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit modi voluptas vero iusto fuga quos totam eius, atis magnam tempora doloribus ducimus dolorem culpa animi beatae tenetur! Sapiente, quia tempora."',
      "/../../assets/images/user2.png",
      "John Doe",
      "Product Manager"
    ),
  ];

  @ViewChild('myCarousel') myCarousel: NguCarousel<any>;
  carouselConfig: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 2, lg: 3, all: 0 },
    load: 3,
    interval: { timing: 3750, initialDelay: 1000 },
    loop: true,
    touch: true,
    velocity: 0.2
  }

  constructor(private cdr: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }
}

class TestimonialCard {
  constructor(
    public logo: string,
    public description: string,
    public image: string,
    public name: string,
    public role: string,
    public hover: boolean = false
  ) { }
}